using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Matrix.DAL.Mongo.Contracts
{
    public interface IMongoEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        string Id { get; set; }
    }
}