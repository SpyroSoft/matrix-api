using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Matrix.DAL.Mongo.Contracts
{
    public interface IRepository<T> where T : class
    {
        Task<IList<T>> GetAll();
        Task<T> GetById(string id);
        Task Update(string id, T entity);
        Task Add(T entity);
        Task<DeleteResult> Remove(string id);
    }
}