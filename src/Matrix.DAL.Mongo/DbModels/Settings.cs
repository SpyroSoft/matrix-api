using Microsoft.Extensions.Configuration;

namespace Matrix.DAL.Mongo.DbModels
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
        public IConfiguration iConfigurationRoot;
    }
}