using System;
using System.Diagnostics;
using System.IO;
using Matrix.DAL.Mongo.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Matrix.DAL.Mongo.DbModels
{
    public class ObjectContext<T> where T : class
    {
        public IConfiguration Configuration {get;set;}
        private readonly IMongoDatabase _database = null;

        public ObjectContext(IOptions<Settings> settings)
        {
            Configuration = settings.Value.iConfigurationRoot; 
            settings.Value.ConnectionString = Configuration.GetSection("MongoConenction:ConnectionString").Value;
            settings.Value.Database = Configuration.GetSection("MongoConenction:Database").Value;

            var client = new MongoClient(settings.Value.ConnectionString);
            if(client != null)
            {
                _database = client.GetDatabase(settings.Value.Database);
            }
        }

        public IMongoCollection<T> EntityCollection
        {
            get
            {
                return _database.GetCollection<T>(typeof(T).Name);
            }
        }

    }
}