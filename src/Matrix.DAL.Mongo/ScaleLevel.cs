namespace Matrix.DAL.Mongo
{
    public enum ScaleLevel
    {
        Low,
        Medium,
        High,
        Expert
    }
}