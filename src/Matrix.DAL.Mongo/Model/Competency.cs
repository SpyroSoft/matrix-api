using System.Collections.Generic;
using Matrix.DAL.Mongo.Contracts;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Matrix.DAL.Mongo.Model
{
    public class Competency : IMongoEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Type")]
        [BsonRepresentation(BsonType.String)]
        public CompetencyType Type { get; set; }

        [BsonElement("ScaleLevel")]
        [BsonRepresentation(BsonType.String)]
        public ScaleLevel ScaleLevel { get; set; }

        [BsonElement("ScaleDescription")]
        public string ScaleDescription { get; set; }
    }
}