using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Matrix.DAL.Mongo.Contracts;
using Matrix.DAL.Mongo.DbModels;
using Matrix.DAL.Mongo.Model;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Matrix.DAL.Mongo.Services
{
    public class Repository<T> : IRepository<T> where T : class, IMongoEntity
    {
        private readonly ObjectContext<T> _context = null;

        public Repository(IOptions<Settings> settings)
        {
            _context = new ObjectContext<T>(settings);
        }

        public async Task<IList<T>> GetAll()
        {
            return await _context.EntityCollection.Find(x => true).ToListAsync();
        }

        public async Task<T> GetById(string id)
        {
            var entity = Builders<T>.Filter.Eq("Id", id);
            return await _context.EntityCollection.Find(entity).FirstOrDefaultAsync();
        }

        public async Task Add(T entity)
        {
            await _context.EntityCollection.InsertOneAsync(entity);
        }

        public Task Update(string id, T entity)
        {
            return _context.EntityCollection.ReplaceOneAsync(x => x.Id == id, entity);
        }

        public async Task<DeleteResult> Remove(string id)
        {
            return await _context.EntityCollection.DeleteOneAsync(Builders<T>.Filter.Eq("Id", id));
        }
    }
}