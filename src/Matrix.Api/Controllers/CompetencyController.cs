using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks;
using Matrix.BL.Contracts;
using Matrix.DAL.Mongo;
using Matrix.DAL.Mongo.Contracts;
using Matrix.DAL.Mongo.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Matrix.Api.Controllers
{
    [Route("api/[controller]")]
    public class CompetencyController : BaseController<Competency>
    {
        private readonly IEntityService<Competency> _competencyService;

        public CompetencyController(IEntityService<Competency> competencyService) : base(competencyService)
        {
        }

        [HttpGet]
        public Task<string> Get()
        {
            return base.GetAllEntities();
        }

        [HttpGet("{id}")] 
        public Task<string> Get(string id) 
        { 
            return base.GetById(id); 
        }

        [HttpPut("{id}")]
        public async Task<string> Put(string id, [FromBody] Competency competency)
        {
            return await base.Put(id, competency);
        }

        [HttpPost]
        public async Task<string> Post([FromBody] Competency competency)
        {
            return await base.Post(competency);
        }

        [HttpDelete("{id}")]
        public async Task<string> Delete(string id)
        {
            return await base.Delete(id);
        }
    }
}