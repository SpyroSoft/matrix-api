using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks;
using Matrix.BL.Contracts;
using Matrix.DAL.Mongo;
using Matrix.DAL.Mongo.Contracts;
using Matrix.DAL.Mongo.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Matrix.Api.Controllers
{
    public class BaseController<T> : Controller where T : class
    {
        private readonly IEntityService<T> _entityService;

        public BaseController(IEntityService<T> entityService)
        {
            _entityService = entityService;
        }

        public async Task<string> GetAllEntities()
        {
            var entities = await _entityService.GetAll();
            return JsonConvert.SerializeObject(entities);
        }

        public async Task<string> GetById(string id) 
        { 
            var entity = await _entityService.GetById(id);
            if(entity != null)
            {
                return JsonConvert.SerializeObject(entity);
            }
            
            return string.Empty;
        }

        public async Task<string> Put(string id, [FromBody] T entity)
        {
            if(string.IsNullOrEmpty(id))
            {
                return "Given ID was invalid.";
            }

            await _entityService.Update(id, entity);
            return "ok";
        }

        public async Task<string> Post([FromBody] T entity)
        {
            await _entityService.Add(entity);
            return "";
        }

        public async Task<string> Delete(string id)
        {
            if(string.IsNullOrEmpty(id))
            {
                return "Given ID was invalid.";
            }

            await _entityService.Delete(id);
            return "";
        }
    }
}