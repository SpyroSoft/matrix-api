﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Matrix.BL.Contracts;
using Matrix.BL.Services;
using Matrix.DAL.Mongo;
using Matrix.DAL.Mongo.Contracts;
using Matrix.DAL.Mongo.DbModels;
using Matrix.DAL.Mongo.Model;
using Matrix.DAL.Mongo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Matrix.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.Configure<Settings>(o => { o.iConfigurationRoot = Configuration; });
            services.AddTransient<IEntityService<Competency>, EntityService<Competency>>();
            services.AddTransient<IRepository<Competency>, Repository<Competency>>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
