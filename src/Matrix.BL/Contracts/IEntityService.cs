using System.Collections.Generic;
using System.Threading.Tasks;
using Matrix.DAL.Mongo.Model;

namespace Matrix.BL.Contracts
{
    public interface IEntityService<T> where T : class
    {
         Task<IList<T>> GetAll();
         Task<T> GetById(string id);
         Task Update(string id, T entity);
         Task Add(T entity);
         Task Delete(string id);
    }
}