using System.Collections.Generic;
using System.Threading.Tasks;
using Matrix.BL.Contracts;
using Matrix.DAL.Mongo.Contracts;
using Matrix.DAL.Mongo.Model;
using Newtonsoft.Json;

namespace Matrix.BL.Services
{
    public class EntityService<T> : IEntityService<T> where T : class
    {
        private readonly IRepository<T> _entityRepository;

        public EntityService(IRepository<T> entityRepository)
        {
            _entityRepository = entityRepository;
        }

        public async Task Add(T entity)
        {
            await _entityRepository.Add(entity);
        }

        public async Task Delete(string id)
        {
            await _entityRepository.Remove(id);
        }

        public async Task<IList<T>> GetAll()
        {
            return await _entityRepository.GetAll();
        }

        public async Task<T> GetById(string id)
        {
            return await _entityRepository.GetById(id);
        }

        public async Task Update(string id, T entity)
        {
            await _entityRepository.Update(id, entity);
        }
    }
}